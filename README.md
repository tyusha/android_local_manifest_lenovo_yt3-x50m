Local manifest to build CyanogenMod 12.1 UNOFFICIAL for [Lenovo Yoga Tab 3 YT3-X50M](https://4pda.to/forum/index.php?showtopic=687964)

Work in progress...


How to download:
-------------

Initialize repo:

    repo init -u git://github.com/LineageOS/android.git -b cm-12.1
    curl --create-dirs -L -o .repo/local_manifests/local_manifest.xml -O -L https://gitlab.com/tyusha/android_local_manifest_lenovo_yt3-x50m/-/raw/cm-12.1/local_manifest.xml
    repo sync


TODO: ---->
-------------

How to compile:
------------

    source ./build/envsetup.sh
    lunch cm_pop35-userdebug
    make otapackage
